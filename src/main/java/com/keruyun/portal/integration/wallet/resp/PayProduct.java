package com.keruyun.portal.integration.wallet.resp;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Data
public class PayProduct {

    public static final String ORG_WEIXINPAY = "ORG_WEIXINPAY";
    public static final String ORG_ALIPAY = "ORG_ALIPAY";

    private String payOrgCode;
    private String payChannelCode;
    private List<String> payModeCodes;

    public boolean weixinPay() {
        return StringUtils.equalsIgnoreCase(payOrgCode, ORG_WEIXINPAY);
    }

    public boolean aliPay() {
        return StringUtils.equalsIgnoreCase(payOrgCode, ORG_ALIPAY);
    }

}
