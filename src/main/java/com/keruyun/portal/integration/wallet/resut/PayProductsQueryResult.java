package com.keruyun.portal.integration.wallet.resut;

import com.keruyun.portal.integration.wallet.resp.PayProduct;
import com.keruyun.portal.integration.wallet.resp.PayProductsQueryResp;

/**
 * <p>
 * Title: PayProductsQueryResult
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wallet.resut
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/9/10
 */
public class PayProductsQueryResult extends WalletResult<PayProductsQueryResp> {

    @Override
    protected boolean isBizSuccess() {
        return super.isBizSuccess() && getContent() != null;
    }

    public boolean supportWeixin() {
        PayProductsQueryResp payProductsQueryResp = getContent();
        if (isSuccess() && payProductsQueryResp.valid()) {
            for (PayProduct payProduct : payProductsQueryResp.getPayProducts()) {
                if (payProduct.weixinPay()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean supportAlipay() {
        PayProductsQueryResp payProductsQueryResp = getContent();
        if (isSuccess() && payProductsQueryResp.valid()) {
            for (PayProduct payProduct : payProductsQueryResp.getPayProducts()) {
                if (payProduct.aliPay()) {
                    return true;
                }
            }
        }
        return false;
    }
}
