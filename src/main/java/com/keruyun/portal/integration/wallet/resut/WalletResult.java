package com.keruyun.portal.integration.wallet.resut;

import com.keruyun.portal.integration.config.AbstractHttpResult;
import lombok.Data;

/**
 * <p>
 * Title: WalletResult
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wallet.resut
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/9/10
 */
@Data
public abstract class WalletResult<T> extends AbstractHttpResult {

    private int status;
    private String message;
    private String msgId;
    private long timestamp;
    private T content;

    @Override
    protected boolean isBizSuccess() {
        return status == 1000;
    }
}
