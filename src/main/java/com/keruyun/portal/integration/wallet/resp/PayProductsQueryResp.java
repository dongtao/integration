package com.keruyun.portal.integration.wallet.resp;

import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

@Data
public class PayProductsQueryResp {

    private long memberId;
    private List<PayProduct> payProducts;

    public boolean valid() {
        return CollectionUtils.isNotEmpty(payProducts);
    }


}
