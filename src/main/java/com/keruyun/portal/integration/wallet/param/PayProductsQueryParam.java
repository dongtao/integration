package com.keruyun.portal.integration.wallet.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import com.keruyun.portal.integration.wallet.resp.PayProduct;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * Title: PayProductsQueryParam
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wallet.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/9/10
 */
@Data
public class PayProductsQueryParam extends AbstractHttpParam {

    private long brandId;
    private long shopId;

    private String userId;
    private String userName;
    private String memberId;

    private List<String> payOrgCodes = Arrays.asList(PayProduct.ORG_ALIPAY, PayProduct.ORG_WEIXINPAY);
    private String source = "portal";

    /**
     * 支付类型：1.线上支付，2.线下支付，不传则不做区分
     */
    private Integer payType;

    public PayProductsQueryParam(long shopId, long brandId) {
        super(DepartmentEnum.WALLET);
        this.shopId = shopId;
        this.brandId = brandId;
    }
}
