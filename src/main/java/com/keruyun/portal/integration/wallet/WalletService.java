package com.keruyun.portal.integration.wallet;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.HttpRequestParam;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.wallet.param.PayProductsQueryParam;
import com.keruyun.portal.integration.wallet.resut.PayProductsQueryResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p>
 * Title: WalletService
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wallet
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/9/10
 */
@Slf4j(topic = "integration")
@Service
public class WalletService {

    @Autowired
    private DomainProperties domainProperties;
    @Autowired
    private HttpClientComponent httpClientComponent;

    /**
     * http://gitlab.shishike.com/checkout/on_check_wiki/wikis/checkout_parameter/member/pay/products/query
     *
     * @param payProductsQueryParam
     */
    public PayProductsQueryResult payProductsQuery(PayProductsQueryParam payProductsQueryParam) {

        HttpRequestParam httpRequestParam = new HttpRequestParam(payProductsQueryParam);
        httpRequestParam.setRequestMethod(RequestMethod.POST);
        httpRequestParam.setUrl(domainProperties.getWallet().api("/member/pay/products/query"));

        ResponseEntity<PayProductsQueryResult> responseEntity =
                httpClientComponent.execute(httpRequestParam, PayProductsQueryResult.class);
        return responseEntity.getBody();
    }
}
