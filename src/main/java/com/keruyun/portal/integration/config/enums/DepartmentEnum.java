package com.keruyun.portal.integration.config.enums;

/**
 * <p>
 * Title: DepartmentEnum
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.config.enums
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/1
 */
public enum DepartmentEnum {
  OT,
  MIND,
  LOYALTY,
  PORTAL,
  WEIXIN,
  SUPPLY,
  WALLET
}
