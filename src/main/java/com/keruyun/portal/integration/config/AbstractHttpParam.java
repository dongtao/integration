package com.keruyun.portal.integration.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.keruyun.portal.integration.config.annotation.HeaderParam;
import com.keruyun.portal.integration.config.annotation.NotUrlParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.util.ReflectionUtils;

/**
 * <p>
 * Title: IRequestParam
 * </p>
 * <p>
 * Description: com.keruyun.weixin.partnerapi
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2017/10/17
 */
public abstract class AbstractHttpParam {

  /**
   * 此次请求所属组织
   */
  @JsonIgnore
  @NotUrlParam
  @Getter
  private DepartmentEnum department;

  public AbstractHttpParam(DepartmentEnum department) {
    this.department = department;
  }

  /**
   * 过滤出头信息字段
   */
  protected static final ReflectionUtils.FieldFilter HEAD_FIELD_FILTER =
      (field) -> field.isAnnotationPresent(HeaderParam.class);

  /**
   * @return
   */
  public HttpHeaders createHttpHeaderFromParam() {
    HttpHeaders httpHeaders = new HttpHeaders();
    ReflectionUtils.doWithFields(headerTargetObject().getClass(), (field) -> {
      HeaderParam headerParam = field.getAnnotation(HeaderParam.class);
      String headerName = StringUtils.defaultIfBlank(headerParam.value(), field.getName());
      if (!field.isAccessible()) {
        field.setAccessible(true);
      }
      httpHeaders.add(headerName, String.valueOf(field.get(headerTargetObject())));
    }, HEAD_FIELD_FILTER);
    return httpHeaders;
  }

  /**
   * 用于获取Header的目标对象。加了@HeaderParam注解的对象模型
   *
   * @return
   */
  protected Object headerTargetObject() {
    return this;
  }
}
