package com.keruyun.portal.integration.config.annotation;

import java.lang.annotation.*;

/**
 * 标识性注解。 所标识的字段，不参与url链接参数拼装
 * <p>
 * Title: NotUrlParam
 * </p>
 * <p>
 * Description: com.calm.v5.partnerapi.config.annotation
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2016年8月10日
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface NotUrlParam {

}
