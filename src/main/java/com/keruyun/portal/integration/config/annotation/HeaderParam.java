package com.keruyun.portal.integration.config.annotation;

import java.lang.annotation.*;

/**
 * <p>
 * Title: HeaderParam
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.config.annotation
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/7/23
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface HeaderParam {
  /**
   * 头信息的Name, 不填写则默认使用字段名称
   */
  String value() default "";
}
