package com.keruyun.portal.integration.config;

/**
 * Http接口响应结果基类
 * <p>
 * Title: AbstractResult
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.config
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2017/12/22
 */
public abstract class AbstractHttpResult {

  /**
   * @return
   */
  public final boolean isSuccess() {
    return isBizSuccess();
  }

  /**
   * 业务意义上的是否成功. 默认为true, 具体业务结果模型自行重写判定业务是否成功
   *
   * @return
   * @author dongt
   * @date 2016年7月29日 下午1:56:35
   */
  protected abstract boolean isBizSuccess();
}
