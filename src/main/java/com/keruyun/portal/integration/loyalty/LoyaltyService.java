package com.keruyun.portal.integration.loyalty;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Title: LoyaltyService
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.loyalty
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2017/12/22
 */
@Service
public class LoyaltyService {

  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

}
