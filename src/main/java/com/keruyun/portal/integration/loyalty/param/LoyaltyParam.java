package com.keruyun.portal.integration.loyalty.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;

/**
 * <p>
 * Title: LoyaltyParam
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.loyalty.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2017/12/22
 */
public class LoyaltyParam extends AbstractHttpParam {
  public LoyaltyParam() {
    super(DepartmentEnum.LOYALTY);
  }
}
