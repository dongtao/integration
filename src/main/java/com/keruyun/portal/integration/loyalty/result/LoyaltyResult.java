package com.keruyun.portal.integration.loyalty.result;

import com.keruyun.portal.integration.config.AbstractHttpResult;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Loyalty接口响应结果抽象模型
 * <p>
 * Title: LoyaltyResult
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.loyalty.param.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2017/12/22
 */
@Data
public abstract class LoyaltyResult<T> extends AbstractHttpResult {
  private T result;
  /**  */
  private int code;
  /** */
  private String errorMessage;

  private String logMessageId;

  @Override
  protected boolean isBizSuccess() {
    return code == 1 && StringUtils.isBlank(errorMessage);
  }
}
