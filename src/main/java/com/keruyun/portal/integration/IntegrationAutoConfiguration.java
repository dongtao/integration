package com.keruyun.portal.integration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = IntegrationAutoConfiguration.class)
public class IntegrationAutoConfiguration {
}

