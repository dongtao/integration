package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

@Data
public class CheckAddItemForWeixinResp {

  private Integer addItemStatus;
  // 不可以加菜,给出提示
  private String msg;
  // 订单ID
  private Long tradeId;
  // 单号,加菜时需要这个字段
  private String tradeNo;
  // 桌台名称,加菜时需要这个字段
  private String tableName;

}
