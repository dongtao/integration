package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TradeItemPropertyResp {

  private Long id;// 服务端自增ID
  private Long tradeItemId;// 服务端自增ID
  private String tradeItemUuid;// 关联TRADE_ITEM的UUID
  private Integer propertyType;// 属性类别：(待口味、做法等统一后可以考虑取消此字段)
  private String propertyUuid;// 属性UUID，对应口味或做法的主键id（uuid）
  private String propertyName;// 属性名称
  private BigDecimal price;// 单价
  private BigDecimal quantity;// 数量
  private BigDecimal amount;// 金额，等于 PRICE * QTY
  private String uuid;// UUID，本笔记录唯一值
  private Integer statusFlag;// 1:VALID:有效的 2:INVALID:无效的
  private static final long serialVersionUID = 1L;
  private String dishPropertyTypeName;
  private Long dishPropertyTypeId;
}
