package com.keruyun.portal.integration.sync.result;

import com.keruyun.portal.integration.sync.resp.AddQueueResp;
import lombok.Data;

@Data
public class AddQueueResult extends SyncResult<AddQueueResp> {

  public long getCommercialID() {
    return getContent().getCommercialID();
  }

  public long getQueueID() {
    return getContent().getQueueID();
  }
}
