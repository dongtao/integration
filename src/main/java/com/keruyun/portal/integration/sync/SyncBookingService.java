package com.keruyun.portal.integration.sync;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.HttpRequestParam;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.sync.param.CancelBookingParam;
import com.keruyun.portal.integration.sync.param.SyncRequestParam;
import com.keruyun.portal.integration.sync.result.CancelBookingResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

@Service
public class SyncBookingService {

  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/v2/booking/booking_cancel
   *
   * @param cancelBookingParam
   * @return
   */
  public CancelBookingResult cancelBooking(CancelBookingParam cancelBookingParam) {
    SyncRequestParam syncRequestParam = new SyncRequestParam(cancelBookingParam);
    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam.setUrl(domainProperties.getSync().api("/portal/v2/booking/cancel"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);
    // 发送请求
    ResponseEntity<CancelBookingResult> response =
        httpClientComponent.execute(httpRequestParam, CancelBookingResult.class);

    CancelBookingResult cancelBookingResult = response.getBody();
    return cancelBookingResult;
  }
}
