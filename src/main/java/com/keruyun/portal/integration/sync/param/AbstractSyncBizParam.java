package com.keruyun.portal.integration.sync.param;

import com.keruyun.portal.integration.config.annotation.HeaderParam;
import com.keruyun.weixin.common.Constants;
import lombok.Getter;

/**
 * <p>
 * Title: AbstractSyncBizParam
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.sync.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/4/2
 */
public abstract class AbstractSyncBizParam {

  @HeaderParam(Constants.KRY_BRAND_ID_KEY)
  @Getter
  private long brandId;
  @Getter
  private long shopId;

  public AbstractSyncBizParam(long brandId, long shopId) {
    this.brandId = brandId;
    this.shopId = shopId;
  }
}
