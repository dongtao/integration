package com.keruyun.portal.integration.sync.param;

import lombok.Data;

@Data
public class CancelQueueParam extends AbstractSyncBizParam {

  private int type = 5;
  private String serverId;
  private long lastSyncMarker;

  public CancelQueueParam(long brandId, long shopId) {
    super(brandId, shopId);
  }
}
