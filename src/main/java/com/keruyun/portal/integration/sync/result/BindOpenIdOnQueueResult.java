package com.keruyun.portal.integration.sync.result;

import com.keruyun.portal.integration.sync.resp.BindOpenIdOnQueueResp;
import lombok.Data;

@Data
public class BindOpenIdOnQueueResult extends SyncResult<BindOpenIdOnQueueResp> {
}
