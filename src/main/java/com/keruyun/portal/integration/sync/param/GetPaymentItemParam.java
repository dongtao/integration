package com.keruyun.portal.integration.sync.param;

import lombok.Data;

/**
 * <p>
 * Title: GetPaymentItemParam
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.sync.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/4/2
 */
@Data
public class GetPaymentItemParam extends AbstractSyncBizParam {

  /**
   * 订单id
   */
  private long tradeId;

  public GetPaymentItemParam(long brandId, long shopId, long tradeId) {
    super(brandId, shopId);
    this.tradeId = tradeId;
  }
}
