package com.keruyun.portal.integration.sync.param;

import lombok.Data;

/**
 * <p>
 * Title: CancelBookingParam
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.sync.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/1
 */
@Data
public class CancelBookingParam extends AbstractSyncBizParam {

  /**
   * 预定ID
   */
  private long orderID;
  /**
   * 用户ID
   */
  private long cancelOrderUser;
  /**
   * 取消原因
   */
  private String reason;

  public CancelBookingParam(long orderID, long cancelOrderUser, long brandId, long shopId) {
    super(brandId, shopId);
    this.orderID = orderID;
    this.cancelOrderUser = cancelOrderUser;
  }


}
