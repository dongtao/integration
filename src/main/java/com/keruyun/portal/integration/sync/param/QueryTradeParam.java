package com.keruyun.portal.integration.sync.param;

import lombok.Data;

@Data
public class QueryTradeParam extends AbstractSyncBizParam {

  private Long tradeId;

  public QueryTradeParam(long brandId, long shopId, long tradeId) {
    super(brandId, shopId);
    this.tradeId = tradeId;
  }
}
