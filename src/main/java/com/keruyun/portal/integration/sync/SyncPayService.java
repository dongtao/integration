package com.keruyun.portal.integration.sync;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.HttpRequestParam;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.sync.param.GetPaymentItemParam;
import com.keruyun.portal.integration.sync.param.SyncRequestParam;
import com.keruyun.portal.integration.sync.param.ZeroTradePayParam;
import com.keruyun.portal.integration.sync.result.GetPaymentItemResult;
import com.keruyun.portal.integration.sync.result.ZeroTradePayResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

@Service("otPayService")
public class SyncPayService {

  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

  @Autowired
  private SyncService syncService;

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/portal/pay_complete
   *
   * @param zeroTradePayParam
   * @return
   */
  public ZeroTradePayResult zeroTradePay(ZeroTradePayParam zeroTradePayParam) {

    SyncRequestParam syncRequestParam = new SyncRequestParam(zeroTradePayParam);
    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam
        .setUrl(domainProperties.getSync().api("/portal/v3/onportal/payment/pay_complete"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);

    ResponseEntity<ZeroTradePayResult> response =
        httpClientComponent.execute(httpRequestParam, ZeroTradePayResult.class);
    ZeroTradePayResult zeroTradePayResult = response.getBody();
    return zeroTradePayResult;
  }

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/doom/portal/getPaymentItemByTradeId
   *
   * @param getPaymentItemParam
   * @return
   */
  public GetPaymentItemResult getPaymentItemResult(GetPaymentItemParam getPaymentItemParam) {
    return syncService.getPaymentItemResult(getPaymentItemParam);
  }
}
