package com.keruyun.portal.integration.sync;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.HttpRequestParam;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.sync.param.AddQueueParam;
import com.keruyun.portal.integration.sync.param.BindOpenIdOnQueueParam;
import com.keruyun.portal.integration.sync.param.CancelQueueParam;
import com.keruyun.portal.integration.sync.param.SyncRequestParam;
import com.keruyun.portal.integration.sync.result.AddQueueResult;
import com.keruyun.portal.integration.sync.result.BindOpenIdOnQueueResult;
import com.keruyun.portal.integration.sync.result.CancelQueueResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

@Service
public class SyncQueueService {

  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/v2/queue/submit
   *
   * @param addQueueParam
   * @return
   */
  public AddQueueResult queue(AddQueueParam addQueueParam) {
    SyncRequestParam syncRequestParam = new SyncRequestParam(addQueueParam);
    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam.setUrl(domainProperties.getSync().api("/portal/v2/queue/submit"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);
    // 发送请求
    ResponseEntity<AddQueueResult> response =
        httpClientComponent.execute(httpRequestParam, AddQueueResult.class);

    AddQueueResult addQueueResult = response.getBody();
    return addQueueResult;
  }

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/v3/queue/bind_open_id.md
   *
   * @param bindOpenIdOnQueueParam
   * @return
   */
  public BindOpenIdOnQueueResult bindOpenIdOnQueue(BindOpenIdOnQueueParam bindOpenIdOnQueueParam) {
    SyncRequestParam syncRequestParam = new SyncRequestParam(bindOpenIdOnQueueParam);
    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam.setUrl(domainProperties.getSync().api("/portal/v3/queue/bind"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);
    // 发送请求
    ResponseEntity<BindOpenIdOnQueueResult> response =
        httpClientComponent.execute(httpRequestParam, BindOpenIdOnQueueResult.class);

    BindOpenIdOnQueueResult bindOpenIdOnQueueResult = response.getBody();
    return bindOpenIdOnQueueResult;
  }

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/v1/queue/changeQueue
   *
   * @return
   */
  public CancelQueueResult cancelQueue(CancelQueueParam cancelQueueParam) {

    SyncRequestParam syncRequestParam = new SyncRequestParam(cancelQueueParam);
    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam.setUrl(domainProperties.getSync().api("/portal/v1/queue/changeQueue"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);
    // 发送请求
    ResponseEntity<CancelQueueResult> response =
        httpClientComponent.execute(httpRequestParam, CancelQueueResult.class);

    CancelQueueResult cancelQueueResult = response.getBody();
    return cancelQueueResult;
  }
}
