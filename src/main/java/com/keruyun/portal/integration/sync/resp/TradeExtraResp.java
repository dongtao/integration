package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Data
public class TradeExtraResp {

  private Long id;// 服务端自增ID
  private Long tradeId;// 交易ID
  private String tradeUuid;// 关联TRADE表的UUID
  private String numberPlate;// 号牌
  private Integer fixType;// 找到桌台的方式：(目前只有微信使用) 1:ASSIGN:服务员分配
  // 2:SCAN_CODE:扫码找位
  private Integer called;// 是否已叫号： 1：未提示 2：已提示
  private String invoiceTitle;// 发票抬头
  private Timestamp expectTime;// 顾客期望的收货时间。 对于外送单据，表示顾客期望送达的时间，为null表示尽快
  // 对于自提单据，表示预约的取货时间

  private String receiverPhone;// 收货人电话

  private String receiverName;// 收货人姓名

  private Integer receiverSex;// 1:MALE 男 2:FEMALE 女

  private Long deliveryAddressId;// 送货地址ID

  private String deliveryAddress;// 送货地址
  private Timestamp receivedTime;// 实际收货时间，为null表示还未送达(或取货) 对于外送单据，表示实际送达的时间
  // 对于自提单据，表示实际来取货的时间

  private BigDecimal deliveryFee;// 送货费

  private String devicePlatform;// 设备平台，比如手持设备等(合作方传来的)

  private String deviceToken;// 设备token，比如手持设备等(合作方传来的)

  private String openIdenty;// 微信号
  private Long userIdenty;// 百度appId
  private String thirdTranNo;// 第三方交易号
  private String deviceIdenty;// 设备标识
  private String deviceNo; // 设备号
  private String uuid;// UUID，本笔记录唯一值
  private Integer statusFlag;// 1:VALID:有效的 2:INVALID:无效的
  // @Column("creator_id")

  private Integer callDishStatus; // 取餐状态： 0未取餐， 1已取餐 默认为0

  private Integer deliveryStatus; // 送餐状态 0 默认等待送餐， 1 正在配送， 2 送餐完成， 3 已清帐 默认为0
  // 

  private String deliveryUserId; // 外卖送餐员的USERID

  private Timestamp deliveryRealTime; // 外卖送餐的真实配送时间

  private Long sendAreaId; // 送餐范围id，到店自提时为空

  private String deliveryMan; // 送餐员

  private Integer orderTip;// 订单提示 0：未提示 1：已提示 默认值为0 微信才用

  private Timestamp bindingDeliveryUserTime; // 绑定外卖员时间

  private Timestamp squareUpTime; // 清帐时间
  private Integer isSubMch; // 是否为受理商户支付 ：0 否，1：是 默认为0 微信才用

  private String serialNumber; // 流水号
  private Integer deliveryPlatform; // 外送平台：1商家自送 2百度外卖
  private Integer hasServing;// 总单服务状态1,未上菜；2，已上菜；3，部分上菜'
  private Integer isPrinted;// =
  // TradeExtraEnum.IsPrinted.UN_PRINTED.getCode();
  // //
  // 总单打印状态，1：未打印，2：已打印,3:部分打印
  /**
   * 服务端最后修改时间.
   */
  private Date serverUpdateTime;
  private String thirdSerialNo;// 第三方订单流水号
}
