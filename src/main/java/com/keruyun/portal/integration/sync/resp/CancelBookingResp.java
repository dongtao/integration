package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

@Data
public class CancelBookingResp {

  /**
   * 订单状态
   */
  private Integer orderStatus;
}
