package com.keruyun.portal.integration.sync.resp;

import com.keruyun.weixin.common.enums.TradeBusinessTypeEnum;
import com.keruyun.weixin.common.enums.TradePayStatusEnum;
import com.keruyun.weixin.common.enums.TradeStatusEnum;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Data
public class TradeResp {
  private Long id; // 服务端自增ID
  private Date bizDate; // 营业日期，由服务端生成
  private Integer domainType; // 所属领域: 1:RESTAURANT:餐饮业
  private TradeBusinessTypeEnum businessType; // 业务形态： 1:SNACK:快餐 2:DINNER:正餐 3:TAKEAWAY:外卖
  // 如果是外卖，那么其交付方式为外送
  // 4:ONLINE_RECHARGE:在线充值 5:POS_PAY:POS支付 6:WEIXIN_PRODUCT:微信商品
  // 7:QUICK_PAY:快捷支付 OnMobile和微信组使用这个值'

  private Integer tradeType; // * 交易类型 1:SELL:售货 2:REFUND:退货 3:SPLIT:拆单

  private Timestamp tradeTime; // * 交易时间

  private TradeStatusEnum tradeStatus; // 交易状态： 1:UNPROCESSED:未处理
  // 2:TEMPORARY:挂单，不需要厨房打印
  // 3:CONFIRMED:已确认(已接受) 4:FINISH:已完成(全部支付) 5:RETURNED:已退货
  // 6:INVALID:已作废 7:REFUSED:已拒绝 8:已取消 10:REPAID:已反结账 11:已挂账

  private TradePayStatusEnum tradePayStatus;

  private Integer deliveryType; // 交付方式： 1:HERE:内用，在店内点餐，可以绑定桌号或者叫餐号
  // 2:SEND:外送，需要填写收货人信息和期望送达时间
  // 3:TAKE:自提，需要填写取货人和约定取货时间 4:CARRY:外带，顾客立即拿走的，以便告知是否打包

  private Integer source; // * 来源: 1android,2ios
  // 3:微信,4:百度外卖,5:百度直达号,6:百度糯米,7:百度地图,8:呼叫中心,9:自助终端,10:商户收银终端,11:商户官网
  // 14:OnMobile快捷支付 15：熟客,16:饿了么 ,17:大众点评

  // 名字待定 1:ANDROID 属于商户收银终端 2:IPAD自主
  // 属于商户自助设备 3:ipad 收银 属于商户收银终端 31:微信官微
  // 属于微信 32:微信商微 属于微信 41:百度外卖 51:百度直达号
  // 61:百度糯米 71:百度地图 81:呼叫中心 111:商户官网
  // ,131:loyal,141:ONMOBILE,151:熟客 33微信快捷支付

  private Integer sourceChild; // *

  private String tradeNo; // * 单号

  private Integer skuKindCount; // * 商品种数，每种商品计1，组合套餐明细不计

  private Double saleAmount; // * 销售金额，明细SALE_AMOUNT之和

  private Double privilegeAmount; // * 各种优惠折扣的减免金额，销货时为负数，退货时为正数

  private Double tradeAmount; // * 交易金额，等于SALE_AMOUNT与PRIVILEGE_AMOUNT之和

  private Double tradeAmountBefore; // * 交易进位处理前金额

  private String tradeMemo; // * 备注

  private Long relateTradeId; // * 1、退货所对应的销货单\r\n2、拆单时对应的原单

  private String relateTradeUuid; // * relateTradeUuid

  private Long brandIdenty; // * 品牌标识

  private Long shopIdenty; // * 门店标识

  private String deviceIdenty; // * 设备标识

  private String uuid; // * UUID，本笔记录唯一值

  private Integer statusFlag; // 1:VALID:有效的 2:INVALID:无效的
  private Integer tradePeopleCount; // 订单就餐人数订单实际就餐人数，放在总表里面,是方便后台根据总金额做统计
  // 默认值为0 该字段为微信使用

  private Integer tradePayForm; // 默认值1 1：线下支付2：在线支付3：组合支付 暂时是微信在使用。 默认值为0
  // 该字段为微信使用

  private Date printTime; // 订单打印时间 默认值为-1 该字段为微信使用

  private Integer actionType; // * 操作类型 操作类型 1为手动 2为自动 默认手动
  private TradeExtraResp tradeExtra; // *
  private List<TradeTableResp> tradeTables; // 订单桌台
  private List<TradeItemResp> tradeItems;// * 订单菜品
  private List<TradePrivilegeResp> tradePrivileges;// * 优惠及附加信息
  //  private List<PaymentItemResp> paymentItems; // *
  private Long creatorId; // *
  private Long updatorId; // *
  private Date serverCreateTime;
  private Date serverUpdateTime; // *
  //  private List<TradeReasonRel> tradeReasonRels; // *
  //  private List<TradeCustomer> tradeCustomers;

  // 1:UNPROCESSED:未处理  2:TEMPORARY:挂单，不需要厨房打印(客户端本地的.)  3:CONFIRMED:已确认  4:FINISH:已完成(全部支付)  5:RETURNED:已退货  6:INVALID:已作废  7:REFUSED:已拒绝,  8:已取消 10:已反结账 11:已挂账 12:已销账 13:待清账',

  /**
   * 订单状态是否是已确认
   *
   * @return
   */
  public boolean tradeStautsConfirmed() {
    return tradeStatus == TradeStatusEnum.CONFIRMED;
  }

  /**
   * 订单是否已完成
   *
   * @return
   */
  public boolean isTradeDone() {
    return tradeStatus == TradeStatusEnum.FINISH;
  }

  /**
   * 订单是否是待确认
   *
   * @return
   */
  public boolean isTradeWaitConfirm() {
    return tradeStatus == TradeStatusEnum.UNPROCESSED;
  }

  /**
   * 订单是否已支付
   *
   * @return
   */
  public boolean isPayed() {
    return tradePayStatus == TradePayStatusEnum.PAID;
  }

  /**
   * 是否是正餐
   *
   * @return
   */
  public boolean dinner() {
    return businessType == TradeBusinessTypeEnum.DINNER;
  }
}
