package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

@Data
public class AddQueueResp {

  private String synFlag;
  private long queueLineId;
  private long queueNumber;
  private long queueID;
  private long commercialID;

}
