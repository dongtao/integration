package com.keruyun.portal.integration.sync.param;

import lombok.Data;

@Data
public class BindOpenIdOnQueueParam extends AbstractSyncBizParam {

  private String openId;
  private Long queueId;

  public BindOpenIdOnQueueParam(long brandId, long shopId) {
    super(brandId, shopId);
  }
}
