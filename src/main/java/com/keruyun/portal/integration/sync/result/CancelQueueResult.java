package com.keruyun.portal.integration.sync.result;

import com.keruyun.portal.integration.sync.resp.CancelQueueResp;
import lombok.Data;

@Data
public class CancelQueueResult extends SyncResult<CancelQueueResp> {
}
