package com.keruyun.portal.integration.sync.param;

import lombok.Data;

@Data
public class NoticeUserLoginParam extends AbstractSyncBizParam {
  /**
   * pos二维码中唯一标识
   */
  private String uuid;
  /**
   * pos二维码中唯一标识
   */
  private String posDeviceID;
  /**
   * 登陆结果
   */
  private boolean result;
  /**
   * 会员ID
   */
  private Long customerID;
  /**
   * 请求来源
   */
  private String appType;

  public NoticeUserLoginParam(long brandId, long shopId) {
    super(brandId, shopId);
  }
}
