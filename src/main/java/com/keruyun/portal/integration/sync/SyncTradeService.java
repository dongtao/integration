package com.keruyun.portal.integration.sync;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.sync.param.QueryTradeParam;
import com.keruyun.portal.integration.sync.param.SyncCheckAddItemForWeixinParam;
import com.keruyun.portal.integration.sync.result.QueryTradeResult;
import com.keruyun.portal.integration.sync.result.SyncCheckAddItemForWeixinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("otTradeService")
public class SyncTradeService {

  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

  @Autowired
  private SyncService syncService;

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/doom/portal/tradeDetail.md
   *
   * @param queryTradeParam
   * @return
   */
  public QueryTradeResult getTradeByIdAndCustomer(QueryTradeParam queryTradeParam) {
    return syncService.getTradeByIdAndCustomer(queryTradeParam);
  }

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/on_mobile/wx_add_item/checkAddItemForWeixin
   *
   * @param syncCheckAddItemForWeixinParam
   * @return
   */
  public SyncCheckAddItemForWeixinResult checkAddItemTS(
      SyncCheckAddItemForWeixinParam syncCheckAddItemForWeixinParam) {
    return syncService.checkAddItemTS(syncCheckAddItemForWeixinParam);
  }
}

