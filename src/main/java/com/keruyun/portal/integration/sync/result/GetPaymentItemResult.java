package com.keruyun.portal.integration.sync.result;

import com.keruyun.portal.integration.sync.resp.GetPaymentItemResp;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Title: GetPaymentItemResult
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.sync.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/4/2
 */
public class GetPaymentItemResult extends SyncResult<List<GetPaymentItemResp>> {

  private static final Predicate<GetPaymentItemResp> PAY_OK_PREDICATE =
      new Predicate<GetPaymentItemResp>() {
        @Override
        public boolean evaluate(GetPaymentItemResp object) {
          return object.payedStatus() && object.aliPay();
        }
      };

  /**
   * 支付宝支付完成的支付记录。 可能是多条,但是只随机提取其中一条。
   *
   * @return
   */
  public GetPaymentItemResp aliPaymentItem() {
    if (getContent() == null) {
      return null;
    }
    if (CollectionUtils.isEmpty(aliPayedItems())) {
      return null;
    }
    return aliPayedItems().get(0);
  }

  /**
   * 过滤出支付状态是已支付 并且是 用支付宝支付 的记录
   *
   * @return 返回已支付的记录
   */
  public List<GetPaymentItemResp> aliPayedItems() {
    List<GetPaymentItemResp> copy = new ArrayList<>();
    copy.addAll(CollectionUtils.emptyIfNull(getContent()));

    CollectionUtils.filter(copy, PAY_OK_PREDICATE);
    return copy;
  }
}
