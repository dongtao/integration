package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

@Data
public class TradeRespWrap {
  /**
   * 同步接口返回的对象
   */
  private TradeResp tradeResponse;
}
