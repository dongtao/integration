package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

@Data
public class GetPaymentItemResp {

  /**
   * 支付明细ID
   */
  private Long id;
  /**
   * 支付方式ID(cashTypeId)：-1:会员卡余额,-2:优惠劵(废弃),-3:现金,-4:银行卡,-5:微信支付,-6:支付宝,-7:百度钱包,-8:百度直达号(停用),-9:积分抵现(废弃),-10:百度地图(停用),-11:银联刷卡,-12:百糯到店付(停用),-13:百度外卖(停用),-14:饿了么(停用),-15:实体卡支付,-16:大众点评(停用),-17:美团外卖(停用),-18:点评团购券(停用),-19:点评闪惠(停用)，-20:临时卡余额 -21:糯米点菜(停用),-22:第三方C端，-23:美团闪惠,-24:美团团购券,-25:钱包生活,-26:百度糯米团购券,-27:乐富支付,-28:熟客,-29:金诚,-30:金诚充值卡
   */
  private Long payModeId;
  private String payModeName;
  /**
   * 支付状态：  1:UNPAID:未支付  2:PAYING:支付中，微信下单选择了在线支付但实际上未完成支付的  (删了)  3:PAID:已支付  4:REFUNDING:退款中  5:REFUNDED:已退款  6:REFUND_FAILED:退款失败  7:PREPAID:预支付(现在都没用)  8:WAITING_REFUND:等待退款 9:PAY_FAIL支付失败 10:REPEAT_PAID重复支付 11:异常支付
   */
  private Integer payStatus;
  /**
   * 支付交易号
   */
  private String payTranNo;
  /**
   * 外部交易号
   */
  private String outTradeNo;
  /**
   * 数据创建时间
   */
  private Long serverCreateTime;

  /**
   * 支付宝支付的用户id
   */
  private String buyerAccount;

  /**
   * 是否是支付成功状态
   *
   * @return
   */
  public boolean payedStatus() {
    return payStatus.intValue() == 3;
  }

  /**
   * 是否是支付宝支付
   *
   * @return
   */
  public boolean aliPay() {
    return payModeId.longValue() == -6;
  }

  /**
   * 是否是微信支付
   *
   * @return
   */
  public boolean weixinPay() {
    return payModeId.longValue() == -5;
  }
}
