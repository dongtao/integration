package com.keruyun.portal.integration.sync.param;

import lombok.Data;

@Data
public class AddQueueParam extends AbstractSyncBizParam {

  public static final int QUEUE_STATUS_PDZ = 0;

  private String syncFlag;
  private String name;
  private int sex;
  private String mobile;
  private int queueStatus = QUEUE_STATUS_PDZ;
  private int repastPersonCount;
  private int isZeroOped = 0;
  private int queueSource = 26;
  private int queueProof = 0;
  private String notifyType = "sms";
  private long customerID;
  private String weixinId;
  public String customerSyncFlag;// 客户唯一标识synFlag

  public AddQueueParam(long brandId, long shopId) {
    super(brandId, shopId);
  }
}
