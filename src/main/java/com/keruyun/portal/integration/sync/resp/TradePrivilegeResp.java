package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class TradePrivilegeResp {

  private Long id;

  /**
   * 交易主单.
   */
  private Long tradeId;

  /**
   * 交易唯一值.
   */
  private String tradeUuid;

  /**
   * 明细UUID.
   */
  private String tradeItemUuid;

  /**
   * 优惠类型.
   */
  private Integer privilegeType;

  /**
   * 优惠值.
   */
  private BigDecimal privilegeValue;

  /**
   * 优惠金额.
   */
  private BigDecimal privilegeAmount;

  /**
   * 优惠id.
   */
  private Long promoId;

  /**
   * 设备标识.
   */
  private String deviceIdenty;

  /**
   * 记录的唯一值.
   */
  private String uuid;

  /**
   * 状态.
   */
  private Integer statusFlag;

  /**
   * 优惠类型名称
   */
  private String privilegeName;
  private Timestamp serverCeateTime;
  ;
  private Timestamp serverUpdateTime;
  /* pad创建时间 */
  private Long clientCreateTime = System.currentTimeMillis();
  /* pad更新时间 */
  private Long clientUpdateTime = clientCreateTime;
}
