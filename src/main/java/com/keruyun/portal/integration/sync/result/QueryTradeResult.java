package com.keruyun.portal.integration.sync.result;

import com.keruyun.portal.integration.sync.resp.TradeRespWrap;

/**
 * <p>
 * Title: QueryTradeResult
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.sync.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/4/2
 */
public class QueryTradeResult extends SyncResult<TradeRespWrap> {

  @Override
  protected boolean isBizSuccess() {
    return super.isBizSuccess() && getContent() != null && getContent().getTradeResponse() != null;
  }

}
