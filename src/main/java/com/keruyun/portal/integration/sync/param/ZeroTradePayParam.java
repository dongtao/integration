package com.keruyun.portal.integration.sync.param;

import lombok.Data;

@Data
public class ZeroTradePayParam extends AbstractSyncBizParam {

  private Long tradeId;
  private Long customerId;
  private Long operateId = -999L;
  private String operateName = "portal";

  public ZeroTradePayParam(long brandId, long shopId) {
    super(brandId, shopId);
  }
}
