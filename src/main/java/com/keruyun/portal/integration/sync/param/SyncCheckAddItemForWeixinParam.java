package com.keruyun.portal.integration.sync.param;

import lombok.Data;

/**
 * <p>
 * Title: SyncCheckAddItemForWeixinParam
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.sync.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/4/2
 */
@Data
public class SyncCheckAddItemForWeixinParam extends AbstractSyncBizParam {

  private long tableId;

  public SyncCheckAddItemForWeixinParam(long brandId, long shopId) {
    super(brandId, shopId);
  }
}
