package com.keruyun.portal.integration.sync;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.HttpRequestParam;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.sync.param.*;
import com.keruyun.portal.integration.sync.result.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

@Service
public class SyncService {

  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/v1/member/loginResult
   *
   * @param noticeUserLoginParam
   * @return
   * @auth lvq
   */
  public NoticeUserLoginResult noticeUserLogin(NoticeUserLoginParam noticeUserLoginParam) {

    SyncRequestParam syncRequestParam = new SyncRequestParam(noticeUserLoginParam);
    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam.setUrl(domainProperties.getSync().api("/portal/v1/member/loginResult"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);

    ResponseEntity<NoticeUserLoginResult> response =
        httpClientComponent.execute(httpRequestParam, NoticeUserLoginResult.class);
    return response.getBody();
  }

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/v1/bellservice/create_call_waiter.md
   *
   * @param callWaiterParam
   * @return
   * @auth lvq
   */
  public CallWaiterResult callWaiter(CallWaiterParam callWaiterParam) {

    SyncRequestParam syncRequestParam = new SyncRequestParam(callWaiterParam);
    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam
        .setUrl(domainProperties.getSync().api("/portal/v1/bellservice/callWaiter/create"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);

    ResponseEntity<CallWaiterResult> response =
        httpClientComponent.execute(httpRequestParam, CallWaiterResult.class);
    return response.getBody();
  }

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/on_mobile/wx_add_item/checkAddItemForWeixin
   * 后续此方法将被废弃, 请调用SyncTradeService的同名方法.
   *
   * @param syncCheckAddItemForWeixinParam
   * @return
   */
  @Deprecated
  public SyncCheckAddItemForWeixinResult checkAddItemTS(
      SyncCheckAddItemForWeixinParam syncCheckAddItemForWeixinParam) {
    SyncRequestParam syncRequestParam = new SyncRequestParam(syncCheckAddItemForWeixinParam);

    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam.setUrl(
        domainProperties.getSync().api("/portal/on_mobile/wx_add_item/check_add_item_for_weixin"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);

    ResponseEntity<SyncCheckAddItemForWeixinResult> response =
        httpClientComponent.execute(httpRequestParam, SyncCheckAddItemForWeixinResult.class);
    return response.getBody();
  }

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/doom/portal/getPaymentItemByTradeId
   * 后续此方法将被废弃, 请调用SyncPayService的同名方法.
   *
   * @param getPaymentItemParam
   * @return
   */
  @Deprecated
  public GetPaymentItemResult getPaymentItemResult(GetPaymentItemParam getPaymentItemParam) {

    SyncRequestParam syncRequestParam = new SyncRequestParam(getPaymentItemParam);

    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam
        .setUrl(domainProperties.getSync().api("/doom/portal/koubei/getPaymentItemByTradeId"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);

    ResponseEntity<GetPaymentItemResult> response =
        httpClientComponent.execute(httpRequestParam, GetPaymentItemResult.class);
    GetPaymentItemResult getPaymentItemResult = response.getBody();

    return getPaymentItemResult;
  }

  /**
   * http://gitlab.shishike.com/sync/api/wikis/api/doom/portal/tradeDetail.md
   * 后续此方法将被废弃, 请调用SyncTradeService的同名方法.
   *
   * @param queryTradeParam
   * @return
   */
  @Deprecated
  public QueryTradeResult getTradeByIdAndCustomer(QueryTradeParam queryTradeParam) {

    SyncRequestParam syncRequestParam = new SyncRequestParam(queryTradeParam);
    HttpRequestParam httpRequestParam = new HttpRequestParam(syncRequestParam);
    httpRequestParam.setUrl(domainProperties.getSync().api("/doom/portal/koubei/trade/detail"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);
    // 发送请求
    ResponseEntity<QueryTradeResult> response =
        httpClientComponent.execute(httpRequestParam, QueryTradeResult.class);

    QueryTradeResult tradeResult = response.getBody();
    return tradeResult;
  }
}
