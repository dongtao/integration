package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

@Data
public class CancelQueueResp {
  private long commercialID;
  /** */
  private String synFlag;
  /**
   * 队列di
   */
  private long queueLineId;
  /**
   * 排队号
   */
  private long queueNumber;
  /**
   * 排队id
   */
  private long queueID;
  /**
   * 姓名
   */
  private String name;
  /**
   * 手机
   */
  private String mobile;
  /**
   * 座机
   */
  private String tel;
  /**
   * 排队状态
   */
  private Integer queueStatus;
  /**
   * 创建时间
   */
  private Long createDateTime;
  /** */
  private Long modifyDateTime;
  /**
   * 排队人数
   */
  private Integer repastPersonCount;
  /**
   * 性别
   */
  private Integer sex;
  /**
   * 排队来源
   */
  private String queueSource;
  /** */
  private String queueProof;
  /** */
  private String inDateTime;
  /** */
  private Integer isZeroOped;
  /**
   * 状态
   */
  private Integer status;
  /**
   * 通知方式
   */
  private String notifyType;
}
