package com.keruyun.portal.integration.sync.resp;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class TradeTableResp {

  private Long id;// 服务端自增ID
  private Long tradeId;// 交易ID“
  private Long tableId;// 关联桌台id
  private String tableName;// tableName
  private String deviceIdenty;// 设备标识
  private String uuid;// UUID，本笔记录唯一值
  private Integer statusFlag;// 1:VALID:有效的 2:INVALID:无效的
  private Long creatorId;// 创建者，创建此记录的系统用户
  private String creatorName;// 创建者姓名
  private Long updatorId;// 最后修改此记录的用户
  private String updatorName;// 最后修改者姓名
  private Long waiterId; // 服务员id
  private String waiterName; // 服务员名称
  private String tradeUuid; // 订单uuid
  private String memo; // 备注
  private Integer selfTableStatus; // =
  // TableEnum.TableStatus.HAVING_MEALS.getType();//DEFAULT
  // '0'
  // COMMENT '桌台的状态,0空闲，1就餐，2待清台' 老数据为NULL
  public Long tableAreaId;
  public String tableAreaName;

  /**
   * 口碑桌台名称
   *
   * @return
   */
  public String koubeiTableNum() {
    return StringUtils.defaultString(tableAreaName).concat("-")
        .concat(StringUtils.defaultString(tableName));
  }


}
