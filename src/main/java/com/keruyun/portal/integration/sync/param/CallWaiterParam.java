package com.keruyun.portal.integration.sync.param;

import com.keruyun.weixin.common.enums.TradeSourceEnum;
import lombok.Data;

@Data
public class CallWaiterParam extends AbstractSyncBizParam {

  private long tableId;
  private String tableName;
  private long tableAreaId;
  private String tableAreaName;
  private Long tradeId;
  private String uuid;
  /**
   * 来源：3:微信，9:自助终端，10:商户收银终端，13:loyal，14:OnMobile，15：熟客,16:饿了么
   */
  private int source = TradeSourceEnum.WEIXIN.getValue();
  private String contentCode;
  private String content;

  public CallWaiterParam(long brandId, long shopId) {
    super(brandId, shopId);
  }
}
