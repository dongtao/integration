package com.keruyun.portal.integration.sync.resp;

import lombok.Data;

@Data
public class BindOpenIdOnQueueResp {
  private boolean result;
}
