package com.keruyun.portal.integration.sync.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import lombok.Getter;

public class SyncRequestParam extends AbstractHttpParam {

  @Getter
  private int userId = -999;
  @Getter
  private long shopID;
  @Getter
  private long brandID;
  @Getter
  private String deviceID = "c-h5";
  @Getter
  private String versionName = "";
  @Getter
  private String versionCode = "";
  @Getter
  private String appType = "1001";
  @Getter
  private String systemType = "c-h5";

  @Getter
  private AbstractSyncBizParam content;

  public SyncRequestParam(AbstractSyncBizParam content) {
    super(DepartmentEnum.OT);
    this.content = content;
    this.shopID = content.getShopId();
    this.brandID = content.getBrandId();
  }

  @Override
  public Object headerTargetObject() {
    return content;
  }
}
