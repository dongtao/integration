package com.keruyun.portal.integration.sync.result;

import com.keruyun.portal.integration.config.AbstractHttpResult;
import lombok.Data;

/**
 * <p>
 * Title: SyncResult
 * </p>
 * <p>
 * Description: com.keruyun.weixin.partnerapi.sync.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2017/10/17
 */
@Data
public class SyncResult<T> extends AbstractHttpResult {

  /**  */
  private T content;

  /**
   * 返回消息
   */
  private String message;
  /**
   * 同步组状态吗
   */
  private int status;
  /**
   * 同步返回的messageId
   */
  private String messageId;

  @Override
  protected boolean isBizSuccess() {
    return status == 1000;
  }
}
