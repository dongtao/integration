package com.keruyun.portal.integration.mind.resp;

import lombok.Data;

@Data
public class CommercialAreaResp {

  private Long id;//           		区域id
  private String areaName;//     		区域名称
  private Long brandId;//      		品牌id
  private Long commercialId;// 		门店id
  private String areaCode;//     		区域编号
  private Integer floor;//        	楼层
  private Integer isSmoking;//    	是否可以吸烟: 0是 1否
  private String memo;//         		备注
  private Long tableTypeId;//  		桌台类型ID

}
