package com.keruyun.portal.integration.mind.resp;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TableResp {

  private Long tableID;// 	桌台Id
  private String tableNum;// 	自定义桌台编号
  private String tableName;// 	桌台名称
  private Long tableTypeId;// 	桌台类型ID
  private Integer tableType;
  // 	桌台类型                         0大厅,           1包厢,    2卡座,      3露台, 4其他
  private Long areaId;// 	区域id
  private Integer tablePersonCount;// 	桌台人数
  private BigDecimal minConsum;// 	最低消费
  private Integer tableStatus;// 	桌台的状态,0空闲，1就餐，2待清台 int(11)3，锁定中
  private Integer canBooking;// 	是否能预订                       0是,             1否
  private Integer sort;// 	顺序
  private String memo;// 	备注
  private Integer status;// 	桌台状态                         0-可用           -1-不可用 ，1未生效，
  //    private String createDateTime;// 	createDateTime
  //    private String modifyDateTime;// 	modifyDateTime
  private Long commercialId;// 	商户id
  private String synFlag;// 	同步标识.                        32位的唯一值.
  private Long queueLineId;// 	排队队列编号
  private String dianCaiOrderId;// 	点菜orderId
  private CommercialAreaResp tableArea;
}
