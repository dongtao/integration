package com.keruyun.portal.integration.mind.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import lombok.Data;

@Data
public class QueryShopTableParam extends AbstractHttpParam {

  private Long shopIdenty;

  public QueryShopTableParam() {
    super(DepartmentEnum.MIND);
  }
}
