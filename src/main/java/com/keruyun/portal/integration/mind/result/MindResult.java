package com.keruyun.portal.integration.mind.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.keruyun.portal.integration.config.AbstractHttpResult;
import lombok.Data;

@Data
public abstract class MindResult<T> extends AbstractHttpResult {
  @JsonProperty("success")
  private boolean callSuccess;
  private Integer code;
  private String message;

  private T data;

  @Override
  protected boolean isBizSuccess() {
    return callSuccess && code == 1000;
  }
}
