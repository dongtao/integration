package com.keruyun.portal.integration.mind.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import lombok.Data;

import java.util.List;

@Data
public class QueryTableBatchParam extends AbstractHttpParam {

  /**
   * 桌台ID
   */
  private List<Long> ids;

  public QueryTableBatchParam() {
    super(DepartmentEnum.MIND);
  }
}
