package com.keruyun.portal.integration.mind.resp;

import com.keruyun.weixin.common.enums.TableTypeEnum;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class Tables {

  private Long tableId;
  private Long shopId;
  private String tableTypeID;
  private String tableName;
  private String tablePosition;
  private String tableDesc;
  private String tableSize;
  private Integer tablePersonCount;
  private Integer status;
  private String createDateTime;
  private String modifyDateTime;
  private String memo;
  private String synFlag;
  private Long tableAraeId;
  private Integer tableStatus;
  private String tableAreaName;
  private TableTypeEnum tableType;

  public String combineAreaTableName() {
    return combineAreaTableName(" ");
  }

  public String combineAreaTableName(String seprator) {
    return StringUtils.defaultString(tableAreaName) + seprator + StringUtils
        .defaultString(tableName);
  }

}
