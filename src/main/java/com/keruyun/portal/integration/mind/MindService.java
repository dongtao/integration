package com.keruyun.portal.integration.mind;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.HttpRequestParam;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.mind.param.GetTableParam;
import com.keruyun.portal.integration.mind.param.QueryShopTableParam;
import com.keruyun.portal.integration.mind.param.QueryTableBatchParam;
import com.keruyun.portal.integration.mind.result.GetTableResult;
import com.keruyun.portal.integration.mind.result.QueryShopTableResult;
import com.keruyun.portal.integration.mind.result.QueryTableBatchResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Service
public class MindService {

  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

  /**
   * http://conf.shishike.com/pages/viewpage.action?pageId=12750290
   *
   * @param getTableParam
   * @return
   */
  public GetTableResult getTable(GetTableParam getTableParam) {

    HttpRequestParam requestParam = new HttpRequestParam(getTableParam);
    requestParam.setUrl(domainProperties.getMind().api("/tableDomain/getPortalTableBySyncFlag"));
    requestParam.setRequestMethod(RequestMethod.POST);

    ResponseEntity<GetTableResult> response =
        httpClientComponent.execute(requestParam, GetTableResult.class);

    GetTableResult getTableResult = response.getBody();
    return getTableResult;
  }

  /**
   * http://gitlab.shishike.com/b-mind/mind/wikis/table_query_byIds
   *
   * @param queryTableBatchParam
   * @return
   */
  public QueryTableBatchResult getTableByIds(QueryTableBatchParam queryTableBatchParam) {

    HttpRequestParam httpRequestParam = new HttpRequestParam(queryTableBatchParam);
    httpRequestParam.setUrl(domainProperties.getMind().api("/tableDomain/getTableByIds"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);

    ResponseEntity<QueryTableBatchResult> responseEntity =
        httpClientComponent.execute(httpRequestParam, QueryTableBatchResult.class);

    return responseEntity.getBody();
  }

  /**
   * http://gitlab.shishike.com/b-mind/mind/wikis/table_query_byIds
   *
   * @param queryShopTableParam
   * @return
   */
  public QueryShopTableResult getTableByShopIdenty(QueryShopTableParam queryShopTableParam) {
    HttpRequestParam httpRequestParam = new HttpRequestParam(queryShopTableParam);
    httpRequestParam.setUrl(domainProperties.getMind().api("/tableDomain/getTableByShopIdenty"));
    httpRequestParam.setRequestMethod(RequestMethod.POST);

    ResponseEntity<QueryShopTableResult> responseEntity =
        httpClientComponent.execute(httpRequestParam, QueryShopTableResult.class);

    return responseEntity.getBody();
  }
}
