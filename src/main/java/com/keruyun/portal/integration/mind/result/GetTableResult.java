package com.keruyun.portal.integration.mind.result;

import com.keruyun.portal.integration.mind.resp.Tables;

/**
 * <p>
 * Title: GetTableResult
 * </p>
 * <p>
 * Description: com.calm.v5.partnerapi.mind.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/5/3
 */
public class GetTableResult extends MindResult<Tables> {

  @Override
  protected boolean isBizSuccess() {
    return super.isBizSuccess() && getData() != null;
  }
}
