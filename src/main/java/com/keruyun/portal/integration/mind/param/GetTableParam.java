package com.keruyun.portal.integration.mind.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;

/**
 * <p>
 * Title: GetTableParam
 * </p>
 * <p>
 * Description: com.calm.v5.partnerapi.mind.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/5/3
 */
public class GetTableParam extends AbstractHttpParam {

  private long brandId;
  private Long shopId;
  private String syncFlag;
  private Long tableId;
  private boolean includeLogicDeleted;

  public GetTableParam(long brandId, Long shopId, String syncFlag) {
    this(brandId, shopId);
    this.syncFlag = syncFlag;
  }

  public GetTableParam(long brandId, Long shopId, long tableId) {
    this(brandId, shopId);
    this.tableId = tableId;
  }

  private GetTableParam(long brandId, Long shopId) {
    super(DepartmentEnum.MIND);
    this.brandId = brandId;
    this.shopId = shopId;
  }

  public long getBrandId() {
    return brandId;
  }

  public void setBrandId(long brandId) {
    this.brandId = brandId;
  }

  public Long getShopId() {
    return shopId;
  }

  public void setShopId(Long shopId) {
    this.shopId = shopId;
  }

  public String getSyncFlag() {
    return syncFlag;
  }

  public void setSyncFlag(String syncFlag) {
    this.syncFlag = syncFlag;
  }

  public Long getTableId() {
    return tableId;
  }

  public void setTableId(Long tableId) {
    this.tableId = tableId;
  }

  public boolean isIncludeLogicDeleted() {
    return includeLogicDeleted;
  }

  public void setIncludeLogicDeleted(boolean includeLogicDeleted) {
    this.includeLogicDeleted = includeLogicDeleted;
  }
}
