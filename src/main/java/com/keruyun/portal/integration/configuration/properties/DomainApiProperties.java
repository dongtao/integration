package com.keruyun.portal.integration.configuration.properties;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * Title: DomainUrlProperties
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.configuration.properties
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2017/12/22
 */
@Data
public class DomainApiProperties {

  /**
   * 域名
   */
  private String domain;

  /**
   * 请求上下文路径
   */
  private String contextPath;

  /**
   * 获取接口链接
   *
   * @param uri
   * @return
   */
  public String api(String uri) {
    return domain + StringUtils.defaultString(contextPath) + uri;
  }

}
