package com.keruyun.portal.integration.configuration.httpclient;

import com.github.kristofa.brave.http.HttpRequest;
import com.github.kristofa.brave.http.SpanNameProvider;

import java.net.URI;

public class UriSpanNameProvider implements SpanNameProvider {
    @Override
    public String spanName(HttpRequest request) {
        URI uri = request.getUri();
        String uriString = uri.toString();
        int querySymboleIndex = uriString.indexOf("?");
        if (querySymboleIndex > 0) {
            return uriString.substring(0, querySymboleIndex);
        }
        return uriString;
    }
}
