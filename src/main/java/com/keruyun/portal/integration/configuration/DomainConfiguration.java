package com.keruyun.portal.integration.configuration;

import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(DomainProperties.class)
public class DomainConfiguration {
}
