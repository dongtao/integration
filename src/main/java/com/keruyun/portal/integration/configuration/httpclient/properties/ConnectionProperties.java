package com.keruyun.portal.integration.configuration.httpclient.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Title: ConnectionProperties
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.configuration.httpclient.properties
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2017/12/21
 */
@Data
@Component
public class ConnectionProperties {

  @Value("${httpclient.connection.connectTimeout:3000}")
  int connectTimeout;
  @Value("${httpclient.so.readTimeout:10000}")
  int readTimeout;
  @Value("${httpclient.connection.connectionRequestTimeout:2000}")
  int connectionRequestTimeout;
  @Value("${httpclient.connection.timeToLive:10}")
  int timeToLive;
  @Value("${httpclient.connection.maxTotal:160}")
  int maxTotal;
  @Value("${httpclient.connection.maxTotalPerRoute:20}")
  int maxTotalPerRoute;

}
