/**
 *
 */
package com.keruyun.portal.integration.configuration.httpclient;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.kristofa.brave.Brave;
import com.github.kristofa.brave.EmptySpanCollectorMetricsHandler;
import com.github.kristofa.brave.Sampler;
import com.github.kristofa.brave.SpanCollector;
import com.github.kristofa.brave.http.HttpSpanCollector;
import com.github.kristofa.brave.httpclient.BraveHttpRequestInterceptor;
import com.github.kristofa.brave.httpclient.BraveHttpResponseInterceptor;
import com.keruyun.portal.integration.configuration.httpclient.properties.ConnectionProperties;
import com.keruyun.portal.integration.configuration.httpclient.properties.TraceProperties;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Title: HttpClientConfiguration
 * </p>
 * <p>
 * Description: com.calm.v5.configuration
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2016年5月24日
 */
@Configuration
public class HttpClientConfiguration {

    @Autowired
    private ConnectionProperties connectionProperties;
    @Autowired
    private TraceProperties traceProperties;

    @Bean
    public RestTemplate getRestTemplate(ClientHttpRequestFactory requestFactory) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(requestFactory);

        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        interceptors.add(new ClientHttpRequestInterceptorImpl());
        restTemplate.setInterceptors(interceptors);

        final MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter =
                new MappingJackson2HttpMessageConverter();
        mappingJacksonHttpMessageConverter.setObjectMapper(objectMapper());
        mappingJacksonHttpMessageConverter.setSupportedMediaTypes(Arrays.asList(
                new MediaType("application", "json", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET),
                // 为了兼容百度SB返回的数据 非JSONP数据格式，contentType也返回的是text/javascript
                new MediaType("text", "javascript", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET),
                new MediaType("application", "octet-stream",
                        MappingJackson2HttpMessageConverter.DEFAULT_CHARSET)

        ));
        restTemplate.setMessageConverters(new ArrayList<HttpMessageConverter<?>>() {
            /** serialVersionUID */
            private static final long serialVersionUID = -7902530205707433058L;

            {
                StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
                stringHttpMessageConverter.setSupportedMediaTypes(Arrays.asList(
                        new MediaType("text", "html", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET),
                        new MediaType("text", "plain", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET),
                        new MediaType("application", "json",
                                MappingJackson2HttpMessageConverter.DEFAULT_CHARSET),
                        new MediaType("application", "octet-stream",
                                MappingJackson2HttpMessageConverter.DEFAULT_CHARSET)));
                add(new ByteArrayHttpMessageConverter());
                add(stringHttpMessageConverter);
                add(new ResourceHttpMessageConverter());
                add(new SourceHttpMessageConverter());
                add(mappingJacksonHttpMessageConverter);
            }
        });
        return restTemplate;
    }

    @Bean
    public ClientHttpRequestFactory getClientHttpRequestFactory(Brave brave) {

        PoolingHttpClientConnectionManager poolingHttpClientConnectionManager =
                new PoolingHttpClientConnectionManager(connectionProperties.getTimeToLive(),
                        TimeUnit.MINUTES);
        poolingHttpClientConnectionManager.setMaxTotal(connectionProperties.getMaxTotal());
        poolingHttpClientConnectionManager
                .setDefaultMaxPerRoute(connectionProperties.getMaxTotalPerRoute());

        HttpClientBuilder httpClientBuilder =
                HttpClientBuilder.create().setConnectionManager(poolingHttpClientConnectionManager);

        if (traceProperties.isEnable()) {
            httpClientBuilder.addInterceptorFirst(new BraveHttpRequestInterceptor(brave.clientRequestInterceptor(), new UriSpanNameProvider()))
                    .addInterceptorFirst(new BraveHttpResponseInterceptor(brave.clientResponseInterceptor()));
        }

        HttpClient httpClient = httpClientBuilder.build();

        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory =
                new HttpComponentsClientHttpRequestFactory(httpClient);
        clientHttpRequestFactory.setConnectTimeout(connectionProperties.getConnectTimeout());
        clientHttpRequestFactory.setReadTimeout(connectionProperties.getReadTimeout());
        clientHttpRequestFactory
                .setConnectionRequestTimeout(connectionProperties.getConnectionRequestTimeout());
        return clientHttpRequestFactory;
    }

    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        return objectMapper;
    }

    @Bean
    public SpanCollector spanCollector() {
        HttpSpanCollector.Config config = HttpSpanCollector.Config.builder().compressionEnabled(false).connectTimeout(5000)
                .flushInterval(1).readTimeout(6000).build();
        return HttpSpanCollector.create(traceProperties.getUrl(), config, new EmptySpanCollectorMetricsHandler());
    }

    @Bean
    public Brave brave(SpanCollector spanCollector) {
        Brave.Builder builder = new Brave.Builder(traceProperties.getServiceName());// 指定serviceName
        builder.spanCollector(spanCollector);
        builder.traceSampler(Sampler.create(traceProperties.getRate()));// 采集率
        return builder.build();
    }

}
