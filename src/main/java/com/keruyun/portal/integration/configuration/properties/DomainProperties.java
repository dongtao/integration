package com.keruyun.portal.integration.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 * Title: DomainProperties
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.configuration.properties
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2017/12/22
 */
@Data
@ConfigurationProperties("domain")
public class DomainProperties {

  private DomainApiProperties gateway;
  private DomainApiProperties infra;
  private DomainApiProperties invoice;
  private DomainApiProperties loyalty;
  private DomainApiProperties mind;
  private DomainApiProperties newLoyalty;
  private DomainApiProperties sync;
  private DomainApiProperties wallet;
  private DomainApiProperties weixin4;
  private DomainApiProperties koubei;
  private DomainApiProperties wxplatform;
  private DomainApiProperties bui;

}
