/**
 *
 */
package com.keruyun.portal.integration.configuration.httpclient;

import com.keruyun.weixin.common.Constants;
import com.keruyun.weixin.common.utils.UUIDUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Title: ClientHttpRequestInterceptorImpl
 * </p>
 * <p>
 * Description: com.calm.v5.common.http
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2016年5月23日
 */
@Slf4j(topic = "integration")
public class ClientHttpRequestInterceptorImpl implements ClientHttpRequestInterceptor {

  /**
   * aplication/json;charset=UTF-8
   */
  private final MediaType application_json_utf8 =
      MediaType.parseMediaType("application/json;charset=UTF-8");

  /*
   * (non-Javadoc)
   *
   * @see
   * org.springframework.http.client.ClientHttpRequestInterceptor#intercept
   * (org.springframework.http.HttpRequest, byte[],
   * org.springframework.http.client.ClientHttpRequestExecution)
   */
  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body,
      ClientHttpRequestExecution execution) throws IOException {

    HttpMethod httpMethod = request.getMethod();
    HttpHeaders headers = request.getHeaders();
    headers.add(Constants.KRY_GLOBAL_MSG_ID, UUIDUtils.getUUID());
    if (httpMethod == HttpMethod.POST) {
      headers.setContentType(application_json_utf8);
    }

    long start = System.currentTimeMillis();
    EnhancerClientHttpResponse resp = null;
    try {
      resp = new EnhancerClientHttpResponse(execution.execute(request, body));
    } finally {

      String respText = resp == null ? "" : resp.responseText();

      log.info(MessageFormat.format("请求地址: {0}, 头信息：{1}, 请求参数: {2} => 返回结果: {3}。 [{4}]ms。 ",
          request.getURI().toString(), headerInfo(request), new String(body), respText,
          System.currentTimeMillis() - start));
    }

    return resp;
  }

  private String headerInfo(HttpRequest request) {
    HttpHeaders httpHeaders = request.getHeaders();
    StringBuilder stringBuilder = new StringBuilder();
    for (Map.Entry<String, List<String>> entry : httpHeaders.entrySet()) {
      stringBuilder.append(entry.getKey()).append(" : ").append(entry.getValue()).append(" ");
    }
    return stringBuilder.toString();
  }

}
