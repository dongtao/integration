package com.keruyun.portal.integration.configuration.httpclient.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class TraceProperties {

    @Value("${trace.zipkin.sample.rate:0.5}")
    private float rate;
    @Value("${trace.zipkin.url:http://172.16.30.27:9411}")
    private String url;
    @Value("${spring.application.name:unknown}")
    private String serviceName;
    @Value("${trace.zipkin.enable: false}")
    private boolean enable;
}
