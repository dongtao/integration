/**
 *
 */
package com.keruyun.portal.integration;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.annotation.NotUrlParam;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldFilter;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Title: HttpClientParam
 * </p>
 * <p>
 * Description: com.calm.v5.common.http
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2016年5月24日
 */
@Data
public class HttpRequestParam {

    /**
     * 请求路径=> http://gitlab.shishike.com/
     */
    protected String url;

    /**
     * 请求方式
     */
    protected RequestMethod requestMethod = RequestMethod.GET;

    /**
     * httpEntity
     */
    protected HttpEntity<AbstractHttpParam> httpEntity;

    /**
     * 是否是resultFull风格请求, 如果是，对于GET方式请求时，buildHttpGetUrl 方法将不再对url进行参数拼接
     */
    protected boolean restFullStyle;

    /**
     * 字段过滤，用于构建Get请求参数的链接模板
     */
    private static FieldFilter fieldFilter = (field) -> {

        // 静态成员 或 被标注NotUrlParam的字段不参与构建链接模板
        return !field.isAnnotationPresent(NotUrlParam.class) && !Modifier
                .isStatic(field.getModifiers());
    };

    /**
     * 构造
     *
     * @param content
     */
    public HttpRequestParam(AbstractHttpParam content) {
        this(content, null);
    }

    /**
     * 构造
     *
     * @param content
     */
    public HttpRequestParam(AbstractHttpParam content, HttpHeaders headers) {
        HttpHeaders httpHeaders = content.createHttpHeaderFromParam();
        if (headers != null) {
            httpHeaders.putAll(headers);
        }
        this.httpEntity = new HttpEntity<>(content, httpHeaders);

    }

    /**
     * @return
     * @author dongt
     * @date 2016年7月19日 下午1:23:40
     */
    public boolean isValid() {
        return StringUtils.isNotBlank(url) && httpEntity.getBody() != null;
    }

    /**
     * @return
     * @author dongt
     * @date 2016年7月19日 下午1:24:47
     */
    public boolean isNotValid() {
        return !isValid();
    }

    /**
     * get方式url链接模板拼装
     *
     * @author dongt
     * @date 2016年8月6日 上午8:08:30
     */
    protected void buildHttpGetUrl() {

        if (requestMethod == RequestMethod.POST || !httpEntity.hasBody()) {
            return;
        }

        if (restFullStyle)
            return;

        final List<String> urlTplParamPairs = new ArrayList<String>();
        ReflectionUtils.doWithFields(httpEntity.getBody().getClass(), (field) -> {

            urlTplParamPairs.add(String.format("%s={%s}", field.getName(), field.getName()));

        }, fieldFilter);

        // 组装好请求参数模板的链接
        url = new StringBuilder(url).append("?").append(StringUtils.join(urlTplParamPairs, "&"))
                .toString();
    }

    public HttpEntity buildNullbodyEntity() {
        return new HttpEntity(null, httpEntity.getHeaders());
    }

}
