package com.keruyun.portal.integration.wxplatform.result;

import com.keruyun.portal.integration.wxplatform.resp.GetUserListData;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * Title: GetUserListResult
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wxplatform.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Data
public class GetUserListResult extends WxPlatformResult {

  /**
   * 关注该公众账号的总用户数
   */
  private int total;
  /**
   * 拉取的OPENID个数，最大值为10000
   */
  private int count;

  /**
   * 列表数据，OPENID的列表
   */
  private GetUserListData data;

  /**
   * 拉取列表的最后一个用户的OPENID
   */
  private String next_openid;

  /**
   * 判断是否还有更多的用户
   *
   * @return
   */
  public boolean hasMore() {
    return StringUtils.isNotBlank(next_openid);
  }

  @Override
  protected boolean isBizSuccess() {
    return super.isBizSuccess() && data != null;
  }
}
