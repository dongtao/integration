package com.keruyun.portal.integration.wxplatform;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.HttpRequestParam;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.wxplatform.param.BatchGetUserInfoParam;
import com.keruyun.portal.integration.wxplatform.param.GetUserInfoParam;
import com.keruyun.portal.integration.wxplatform.param.GetUserListParam;
import com.keruyun.portal.integration.wxplatform.result.BatchGetUserInfoResult;
import com.keruyun.portal.integration.wxplatform.result.GetUserInfoResult;
import com.keruyun.portal.integration.wxplatform.result.GetUserListResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p>
 * Title: WeixinPlatformService
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wxplatform
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Service
public class WeixinPlatformService {

  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

  /**
   * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140840
   *
   * @param getUserListParam
   */
  public GetUserListResult getUserList(GetUserListParam getUserListParam) {
    HttpRequestParam httpRequestParam = new HttpRequestParam(getUserListParam);
    httpRequestParam.setUrl(domainProperties.getWxplatform().api("/user/get"));
    httpRequestParam.setRequestMethod(RequestMethod.GET);

    ResponseEntity<GetUserListResult> response =
        httpClientComponent.execute(httpRequestParam, GetUserListResult.class);

    return response.getBody();
  }

  /**
   * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140839
   *
   * @param getUserInfoParam
   * @return
   */
  public GetUserInfoResult getUserInfo(GetUserInfoParam getUserInfoParam) {
    HttpRequestParam httpRequestParam = new HttpRequestParam(getUserInfoParam);
    httpRequestParam.setUrl(domainProperties.getWxplatform().api("/user/info"));
    httpRequestParam.setRequestMethod(RequestMethod.GET);

    ResponseEntity<GetUserInfoResult> response =
        httpClientComponent.execute(httpRequestParam, GetUserInfoResult.class);

    return response.getBody();
  }

  /**
   * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140839
   *
   * @param batchGetUserInfoParam
   * @return
   */
  public BatchGetUserInfoResult batchGetUserInfoResult(
      BatchGetUserInfoParam batchGetUserInfoParam) {
    HttpRequestParam httpRequestParam = new HttpRequestParam(batchGetUserInfoParam);
    httpRequestParam.setUrl(domainProperties.getWxplatform()
        .api("/user/info/batchget?access_token=" + batchGetUserInfoParam.getAccessToken()));
    httpRequestParam.setRequestMethod(RequestMethod.POST);

    ResponseEntity<BatchGetUserInfoResult> response =
        httpClientComponent.execute(httpRequestParam, BatchGetUserInfoResult.class);

    return response.getBody();
  }
}
