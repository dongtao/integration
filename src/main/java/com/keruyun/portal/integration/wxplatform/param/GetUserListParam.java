package com.keruyun.portal.integration.wxplatform.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import lombok.Data;

@Data
public class GetUserListParam extends AbstractHttpParam {

  /**
   * 调用接口凭证
   */
  private String access_token;

  /**
   * 第一个拉取的OPENID，不填默认从头开始拉取
   */
  private String next_openid;

  public GetUserListParam() {
    super(DepartmentEnum.WEIXIN);
  }
}
