package com.keruyun.portal.integration.wxplatform.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import lombok.Getter;

/**
 * <p>
 * Title: UserInfoParam
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wxplatform.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
public class UserInfoParam extends AbstractHttpParam {
  /**
   * 普通用户的标识，对当前公众号唯一
   */
  @Getter
  private String openid;
  /**
   * 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
   */
  @Getter
  private String lang = "zh_CN";

  public UserInfoParam(String openId) {
    super(DepartmentEnum.WEIXIN);
    this.openid = openId;
  }
}
