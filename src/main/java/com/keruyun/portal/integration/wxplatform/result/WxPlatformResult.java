package com.keruyun.portal.integration.wxplatform.result;

import com.keruyun.portal.integration.config.AbstractHttpResult;
import lombok.Data;

/**
 * <p>
 * Title: WxPlatformResult
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wxplatform
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Data
public abstract class WxPlatformResult extends AbstractHttpResult {

  private int errcode;

  private String errmsg;

  @Override
  protected boolean isBizSuccess() {
    return errcode == 0;
  }
}
