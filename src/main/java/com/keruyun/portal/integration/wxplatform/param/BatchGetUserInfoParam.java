package com.keruyun.portal.integration.wxplatform.param;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Title: BatchGetUserInfoParam
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wxplatform.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Data
public class BatchGetUserInfoParam extends AbstractHttpParam {

  @JsonIgnore
  private String accessToken;

  private List<UserInfoParam> user_list = new ArrayList<>();

  public BatchGetUserInfoParam() {
    super(DepartmentEnum.WEIXIN);
  }

  public void initUserList(List<String> openIds) {
    for (String openId : openIds) {
      user_list.add(new UserInfoParam(openId));
    }
  }
}
