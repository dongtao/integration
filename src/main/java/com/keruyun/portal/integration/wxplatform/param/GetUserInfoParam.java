package com.keruyun.portal.integration.wxplatform.param;

import lombok.Data;

/**
 * <p>
 * Title: GetUserInfoParam
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wxplatform.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Data
public class GetUserInfoParam extends UserInfoParam {
  /**
   * 调用接口凭证
   */
  private String access_token;

  public GetUserInfoParam(String openId) {
    super(openId);
  }
}
