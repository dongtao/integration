package com.keruyun.portal.integration.wxplatform.resp;

import lombok.Data;

import java.util.List;

/**
 * <p>
 * Title: GetUserListData
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wxplatform.resp
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Data
public class GetUserListData {

  private List<String> openid;
}
