package com.keruyun.portal.integration.wxplatform.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * Title: BatchGetUserInfoResult
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.wxplatform.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Data
public class BatchGetUserInfoResult extends WxPlatformResult {

  @JsonProperty("user_info_list")
  private List<GetUserInfoResult> userInfoList;
}
