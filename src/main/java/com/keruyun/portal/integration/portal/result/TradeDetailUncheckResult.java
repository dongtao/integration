package com.keruyun.portal.integration.portal.result;

import java.util.Map;

/**
 * <p>
 * Title: TradeDetailUncheckResult
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.portal.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/2/27
 */
public class TradeDetailUncheckResult extends PortalResult<Map> {
  @Override
  protected boolean isBizSuccess() {
    return super.isBizSuccess() && getData() != null;
  }
}
