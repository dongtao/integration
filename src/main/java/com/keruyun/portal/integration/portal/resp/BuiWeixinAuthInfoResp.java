package com.keruyun.portal.integration.portal.resp;

import lombok.Data;

@Data
public class BuiWeixinAuthInfoResp {

  private String access_token;
  private String component_ticket;
  private String platform_appid;

}
