package com.keruyun.portal.integration.portal.resp;

import lombok.Data;

@Data
public class BuiWeixinAccessTokenResp {

  private String access_token;
  //
  private String appId;
  //
  private String biz_appid;
  //
  private String biz_token;
  //
  private String component_ticket;
  /**
   * 品牌id
   */
  private Long biz_id;
  private String platform_appid;
  
}
