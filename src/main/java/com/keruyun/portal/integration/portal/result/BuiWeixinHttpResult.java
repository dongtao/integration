package com.keruyun.portal.integration.portal.result;

import com.keruyun.portal.integration.config.AbstractHttpResult;
import lombok.Data;

/**
 * <p>
 * Title: BuiWeixinHttpResult
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.portal.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Data
public abstract class BuiWeixinHttpResult<T> extends AbstractHttpResult {
  /**  */
  private T data;

  /**
   * 返回消息
   */
  private String msg;
  /**
   * 状态码
   */
  private int code;
  /**
   * msgId
   */
  private String mdcTarget;

  private String msgId;
  /**
   * timestamp
   */
  private String timestamp;

  @Override
  protected boolean isBizSuccess() {
    return code == 200;
  }
}
