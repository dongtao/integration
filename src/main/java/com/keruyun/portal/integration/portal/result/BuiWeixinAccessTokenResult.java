package com.keruyun.portal.integration.portal.result;

import com.keruyun.portal.integration.portal.resp.BuiWeixinAccessTokenResp;

/**
 * <p>
 * Title: BuiWeixinAccessTokenResult
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.portal.result
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
public class BuiWeixinAccessTokenResult extends BuiWeixinHttpResult<BuiWeixinAccessTokenResp> {

  @Override
  public boolean isBizSuccess() {
    return super.isBizSuccess() && null != getData() && null != getData().getBiz_token();
  }

  @Override
  public String getMsg() {
    if (!isBizSuccess()) {
      return "获取token失败:" + super.getMsg();
    }
    return super.getMsg();
  }
}
