package com.keruyun.portal.integration.portal;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.HttpRequestParam;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.portal.param.BuiWeixinAccessTokenParam;
import com.keruyun.portal.integration.portal.result.BuiWeixinAccessTokenResult;
import com.keruyun.portal.integration.portal.result.BuiWeixinAuthInfoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p>
 * Title: PortalBuiService
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.portal
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Service
public class PortalBuiService {
  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

  /**
   * @param param
   * @return
   */
  public BuiWeixinAuthInfoResult getWeixinAccessToken(BuiWeixinAccessTokenParam param) {
    HttpRequestParam httpRequestParam = new HttpRequestParam(param);
    httpRequestParam.setUrl(domainProperties.getBui().api("/wechat/auth"));
    httpRequestParam.setRequestMethod(RequestMethod.GET);

    ResponseEntity<BuiWeixinAuthInfoResult> response =
        httpClientComponent.execute(httpRequestParam, BuiWeixinAuthInfoResult.class);
    BuiWeixinAuthInfoResult buiWeixinAuthInfoResult = response.getBody();

    return buiWeixinAuthInfoResult;
  }

  /**
   * @param param
   * @return
   */
  public BuiWeixinAccessTokenResult getAccessTokenByBrand(BuiWeixinAccessTokenParam param) {
    HttpRequestParam httpRequestParam = new HttpRequestParam(param);
    httpRequestParam.setRestFullStyle(true);
    httpRequestParam.setUrl(domainProperties.getBui().api("/wechat/{brandId}"));
    httpRequestParam.setRequestMethod(RequestMethod.GET);

    ResponseEntity<BuiWeixinAccessTokenResult> response =
        httpClientComponent.execute(httpRequestParam, BuiWeixinAccessTokenResult.class);
    BuiWeixinAccessTokenResult buiWeixinAuthInfoResult = response.getBody();

    return buiWeixinAuthInfoResult;
  }
}
