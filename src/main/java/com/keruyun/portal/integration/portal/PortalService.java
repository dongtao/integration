package com.keruyun.portal.integration.portal;

import com.keruyun.portal.integration.HttpClientComponent;
import com.keruyun.portal.integration.HttpRequestParam;
import com.keruyun.portal.integration.configuration.properties.DomainProperties;
import com.keruyun.portal.integration.portal.param.TradeDetailUncheckParam;
import com.keruyun.portal.integration.portal.result.TradeDetailUncheckResult;
import com.keruyun.weixin.common.exceptions.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j(topic = "integration")
@Service
public class PortalService {

  @Autowired
  private HttpClientComponent httpClientComponent;

  @Autowired
  private DomainProperties domainProperties;

  /**
   * 结算前订单详情页
   *
   * @param tradeDetailUncheckParam
   * @return
   */
  public TradeDetailUncheckResult tradeDetailUncheck(
      TradeDetailUncheckParam tradeDetailUncheckParam) {

    HttpRequestParam httpRequestParam = new HttpRequestParam(tradeDetailUncheckParam);
    httpRequestParam.setRequestMethod(RequestMethod.POST);
    httpRequestParam.setUrl(domainProperties.getWeixin4()
        .api("/order/tradeDetailUncheck.json?shopId=" + tradeDetailUncheckParam.getShopId()));

    ResponseEntity<TradeDetailUncheckResult> responseEntity = null;
    try {
      responseEntity =
          httpClientComponent.execute(httpRequestParam, TradeDetailUncheckResult.class);
    } catch (BusinessException e) {
      return new TradeDetailUncheckResult();
    }

    return responseEntity.getBody();
  }

}
