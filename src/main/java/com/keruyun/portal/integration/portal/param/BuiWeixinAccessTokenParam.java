package com.keruyun.portal.integration.portal.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import lombok.Data;

/**
 * <p>
 * Title: BuiWeixinAccessTokenParam
 * </p>
 * <p>
 * Description: com.keruyun.portal.integration.portal.param
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/8/10
 */
@Data
public class BuiWeixinAccessTokenParam extends AbstractHttpParam {

  private long brandId;

  public BuiWeixinAccessTokenParam() {
    super(DepartmentEnum.PORTAL);
  }
}
