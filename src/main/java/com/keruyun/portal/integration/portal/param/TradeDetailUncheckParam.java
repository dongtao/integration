package com.keruyun.portal.integration.portal.param;

import com.keruyun.portal.integration.config.AbstractHttpParam;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import lombok.Data;

@Data
public class TradeDetailUncheckParam extends AbstractHttpParam {

  private Long shopId;
  private Long brandId;
  private Long orderId;

  public TradeDetailUncheckParam() {
    super(DepartmentEnum.PORTAL);
  }
}
