package com.keruyun.portal.integration.portal.result;

import com.keruyun.portal.integration.config.AbstractHttpResult;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public abstract class PortalResult<T> extends AbstractHttpResult {

  private T data;

  private String msg;

  private String code;

  private String msgKey;

  @Override
  protected boolean isBizSuccess() {
    return StringUtils.equals("200", code);
  }
}
