package com.keruyun.portal.integration;

import com.keruyun.portal.integration.config.AbstractHttpResult;
import com.keruyun.portal.integration.config.enums.DepartmentEnum;
import com.keruyun.portal.integration.exceptions.ApiCallException;
import com.keruyun.weixin.common.utils.BeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;

@Slf4j(topic = "integration")
@Component
public class HttpClientComponent {

    @Autowired
    private RestTemplate restTemplate;

    /**
     * @param httpRequestParam
     * @param clazz            结果数据模型
     * @return
     * @author dongt
     * @date 2016年5月26日 下午2:27:27
     */
    public <T extends AbstractHttpResult> ResponseEntity execute(HttpRequestParam httpRequestParam,
                                                                 Class<T> clazz) {

        if (httpRequestParam.isNotValid()) {
            throw new ApiCallException("接口调用参数异常.");
        }

        ResponseEntity<T> response = null;
        try {
            if (httpRequestParam.requestMethod == RequestMethod.POST) {
                response =
                        restTemplate.postForEntity(httpRequestParam.url, httpRequestParam.httpEntity, clazz);
            } else if (httpRequestParam.requestMethod == RequestMethod.GET) {
                httpRequestParam.buildHttpGetUrl();
                response = restTemplate.exchange(httpRequestParam.url, HttpMethod.GET, httpRequestParam.buildNullbodyEntity(), clazz, BeanUtil.toMap(httpRequestParam.httpEntity.getBody()));
            }
        } catch (Exception e) {
            DepartmentEnum department = httpRequestParam.getHttpEntity().getBody().getDepartment();
            log.error(MessageFormat.format("调用{0}接口失败! => {1}", department.name(), httpRequestParam.url),
                    e);
            throw new ApiCallException(department.name() + "组接口异常", e);
        }

        return response;
    }

}
