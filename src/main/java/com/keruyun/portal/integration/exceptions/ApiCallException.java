package com.keruyun.portal.integration.exceptions;

/**
 * <p>
 * Title: ApiCallException
 * </p>
 * <p>
 * Description: com.keruyun.weixin.integration.exceptions
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: 客如云
 * </p>
 *
 * @author dongt
 * @date 2018/7/25
 */
public class ApiCallException extends RuntimeException {

  public ApiCallException(String message) {
    super(message);
  }

  public ApiCallException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
